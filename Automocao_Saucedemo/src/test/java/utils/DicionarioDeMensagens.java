package utils;

public class DicionarioDeMensagens {
	
	public String msgBlock = "Epic sadface: Sorry, this user has been locked out.";
	public String msgInvalido = "Epic sadface: Username and password do not match any user in this service";
	public String msgBranco = "Epic sadface: Username is required";
	public String msgFirstNameRequired = "Error: First Name is required";
	public String msgCompra = "Thank you for your order!";
}
