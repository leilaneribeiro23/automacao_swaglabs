package runner;

import org.junit.runner.RunWith;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import driver.Drivers;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;
import io.github.bonigarcia.wdm.WebDriverManager;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features", glue = "steps", tags = "@login", dryRun = false, monochrome = true, plugin = {
		"pretty", "html:target/report-cucumber.html" }, snippets = SnippetType.CAMELCASE

)

public class Executa extends Drivers {

	public static void abrirNavegador() {
		String url = "https://www.saucedemo.com";
		String navegador = "Chrome";
		
        if (navegador.equalsIgnoreCase("Chrome")) {
        	WebDriverManager.chromedriver().setup();
    		driver = new ChromeDriver();
		}else {
		if (navegador.equalsIgnoreCase("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			
		}
		}
		driver.manage().window().maximize();
		driver.get(url);
	}

	public static void fecharNavegador() {
		driver.quit();
	}

}
