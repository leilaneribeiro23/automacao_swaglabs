# Automação de Testes - Automacao_Saucedemo

Este projeto é um exemplo de automação de testes de interface utilizando Java, Selenium WebDriver, Cucumber e JUnit. O objetivo deste projeto é automatizar cenários de teste relacionados à funcionalidade do carrinho, compra e login em um site de e-commerce chamado SauceDemo.

## Pré-requisitos

Certifique-se de ter as seguintes ferramentas instaladas em sua máquina:

-   Java Development Kit (JDK)
-   Maven

## Configuração do Ambiente

1.  Clone ou faça o download deste repositório para sua máquina.
2.  Importe o projeto em sua IDE favorita como um projeto Maven.
3.  No arquivo `pom.xml`, as dependências necessárias, incluindo o Selenium WebDriver, Cucumber e JUnit, já estão configuradas.
4.  No arquivo `Executa.java`, você encontrará a classe principal responsável por executar os testes automatizados. Certifique-se de ter o WebDriver apropriado instalado (Chrome ou Firefox).
5.  As definições dos cenários de teste podem ser encontradas nos arquivos de feature do Cucumber no diretório `src/test/resources/features`.
    

## Execução dos Testes

Para executar os testes automatizados neste projeto, siga as etapas abaixo:

1. Abra a classe `Executa` no pacote `runner` localizada em `src/test/java/runner/Executa.java`.

2. No bloco `@CucumberOptions`, você verá a opção `tags = "@login"`. Substitua `"@login"` pela tag correspondente ao teste que deseja executar:
   - Para executar os testes de login, utilize `@login`
   - Para executar os testes do carrinho, utilize `@carrinho`
   - Para executar os testes de compra, utilize `@compra`

3. Execute a classe `Executa` como um teste JUnit. Você pode fazer isso clicando com o botão direito do mouse sobre a classe no IDE e selecionando "Run As" (Executar Como) > "JUnit Test".

4. Durante a execução dos testes, você poderá acompanhar o progresso no console do seu IDE. Os testes serão executados no navegador Chrome por padrão. Caso não seja possível abrir o Chrome, o Firefox será usado como alternativa.

5. Após a conclusão dos testes, um relatório detalhado será gerado e salvo no formato HTML na pasta `target/report-cucumber.html`.

6. Para visualizar o relatório, siga estas etapas:
   - Navegue até a pasta `target` na raiz do projeto.
   - Localize o arquivo `report-cucumber.html`.
   - Clique com o botão direito do mouse no arquivo e selecione "Open With" (Abrir com).
   - Escolha um navegador da web instalado em seu computador (por exemplo, Google Chrome, Mozilla Firefox, etc.).

7. O navegador abrirá o arquivo `report-cucumber.html` e exibirá o relatório do Cucumber com os resultados dos testes automatizados realizados no projeto.


## Estrutura do Projeto

-   `src/test/java/driver/Drivers.java`: Classe base para configuração do WebDriver.
- `src/test/java/elementos/Elementos.java`: Classe que contém mapeamentos de elementos do site. 
- `src/test/java/metodos/Metodos.java`: Classe com métodos de interação com os elementos da página. 
-   `src/test/java/runner/Executa.java`: Classe de execução dos testes, configura o WebDriver e define as opções do Cucumber.
- `src/test/resources/steps`: Diretório contendo as classes de definição de passos do Cucumber.
-  `src/test/java/utils/DicionarioDeMensagens.java`: Classe com mensagens de erro e alerta para os testes.
- `src/test/java/utils/InformacoesProdutos.java`: Classe com informações dos produtos para testes.
- `src/test/java/utils/MassaDeDados.java`: Classe com dados de teste.
-   `src/test/resources/features`: Diretório contendo os arquivos de features do Cucumber.

## Cenários de Teste

O projeto inclui cenários de teste organizados em features do Cucumber, com foco nas funcionalidades de carrinho, compra e login. Cada feature tem cenários que cobrem diferentes casos de uso.

## Relatórios e evidências

Os relatórios de teste são gerados automaticamente na pasta `target` após a execução dos testes. Você encontrará os relatórios em formato HTML em `target/report-cucumber.html`.

As  evidências  dos  testes  estão  armazenadas  na  pasta  `evidências`  na  raiz  do  projeto.

## Contato

Se você tiver alguma dúvida ou sugestão sobre este projeto, sinta-se à vontade para entrar em contato.

----------
