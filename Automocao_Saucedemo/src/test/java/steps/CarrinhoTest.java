package steps;

import org.junit.After;
import org.junit.runner.RunWith;

import elementos.Elementos;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;
import metodos.Metodos;
import runner.Executa;
import utils.InformacoesProdutos;
import utils.MassaDeDados;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features", glue = "steps", tags = "@carrinho", dryRun = false, monochrome = true, plugin = {
		"pretty", "html:target/report-cucumber.html" }, snippets = SnippetType.CAMELCASE

)

public class CarrinhoTest {
	
	Metodos metodos = new Metodos();
	Elementos el = new Elementos();
	MassaDeDados massa = new MassaDeDados();
	InformacoesProdutos inf = new InformacoesProdutos();

	@Given("que esteja na tela de produtos")
	public void queEstejaNaTelaDeProdutos() {
		Executa.abrirNavegador();
		metodos.escrever(el.name, massa.standard_user);
		metodos.escrever(el.password, massa.secret_sauce);
		metodos.clicar(el.btnLogin);

	}
	
	@After
	public void finalizarTeste() {
		Executa.fecharNavegador();
	}

	@Given("clicar no produto backpack")
	public void clicarNoProdutoBackpack() {
		metodos.clicar(el.backPack);
	}

	@Given("validar as informacoes do produto backpack")
	public void validarAsInformacoesDoProdutoBackpack() {
		metodos.validarTexto(el.infoProdutos, inf.infoBackPack);
	}

	@Given("clicar em adicionar produto no carrinho")
	public void clicarEmAdicionarProdutoNoCarrinho() {
		metodos.clicar(el.addBackPack);
	}

	@When("clicar no botao carrinho")
	public void clicarNoBotaoCarrinho() {
		metodos.clicar(el.btnCarrinho);
	}

	@Then("carrinho exibe produto adicionado")
	public void carrinhoExibeProdutoAdicionado() {
		metodos.evidencia("Adicionar Produto no carrinho");
	}

	@Given("clicar no produto tshirt red")
	public void clicarNoProdutoTshirtRed() {
		metodos.clicar(el.tShirtRed);
	}

	@Given("validar as informacoes do produto tshirt red")
	public void validarAsInformacoesDoProdutoTshirtRed() {
		metodos.validarTexto(el.infoProdutos, inf.infoTShirtRed);
	}

	@When("clicar no botao continuar compra")
	public void clicarNoBotaoContinuarCompra() {
		metodos.clicar(el.btnContinueShopping);
	}

	@Then("sou redirecionado para pagina inicial")
	public void souRedirecionadoParaPaginaInicial() {
		metodos.validarTexto(el.products, "Products");
	}

	@Given("adicionar produtos no carrinho")
	public void adicionarProdutosNoCarrinho() {
		metodos.clicar(el.addBackPack);
		metodos.clicar(el.addFleeceJacket);
	}

	@When("clicar no botao remover")
	public void clicarNoBotaoRemover() {
		metodos.clicar(el.removeBackPack);
	}

	@Then("carrinho atualizado com sucesso")
	public void carrinhoAtualizadoComSucesso() {
		metodos.evidencia("Remover produto do carrinho");
	}

}
