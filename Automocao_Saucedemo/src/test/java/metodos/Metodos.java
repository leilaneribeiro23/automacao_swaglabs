package metodos;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import driver.Drivers;

public class Metodos extends Drivers {

	public void escrever(By elemento, String texto) {
		try {
			driver.findElement(elemento).sendKeys(texto);
		} catch (Exception e) {
			System.err.println("Erro ao tentar escrever no elemento" + elemento);
		}

	}

	public void clicar(By elemento) {
		try {
			driver.findElement(elemento).click();
		} catch (Exception e) {
			System.err.println("Erro ao tentar escrever no elemento" + elemento);
		}
	}

	
	public void currentUrl(String urlDesejada) {
		try {
			assertEquals(urlDesejada, driver.getCurrentUrl());
		} catch (Exception e) {
			System.err.println("Erro ao tentar validar a urlDesejada" + urlDesejada);
		}
		
	}
	
	public void validarTexto(By elemento, String mensagemEsperada) {
		try {
			String msgCapturada = driver.findElement(elemento).getText();
			assertEquals(mensagemEsperada, msgCapturada);
		} catch (Exception e) {
			System.err.println("Erro ao tentar validar a mensagem" + mensagemEsperada);
		}
	}
	
	public void evidencia(String nomeEvidencia) {
		TakesScreenshot scrShot = (TakesScreenshot) driver;
		File srcFile = scrShot.getScreenshotAs(OutputType.FILE);
		File destFile = new File("./evidencias/" + nomeEvidencia + ".png");
		try {
			FileUtils.copyFile(srcFile, destFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String obterValorAtributo(By elemento, String string) {
		 return driver.findElement(elemento).getText();
	
	}



}
