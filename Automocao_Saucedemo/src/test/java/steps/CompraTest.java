package steps;

import elementos.Elementos;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import metodos.Metodos;
import runner.Executa;
import utils.DicionarioDeMensagens;
import utils.InformacoesProdutos;
import utils.MassaDeDados;

public class CompraTest {

	Metodos metodos = new Metodos();
	Elementos el = new Elementos();
	MassaDeDados massa = new MassaDeDados();
	InformacoesProdutos inf = new InformacoesProdutos();
	DicionarioDeMensagens msg = new DicionarioDeMensagens();

	@Given("que esteja logado no site")
	public void queEstejaLogadoNoSite() {
		Executa.abrirNavegador();
		metodos.escrever(el.name, massa.standard_user);
		metodos.escrever(el.password, massa.secret_sauce);
		metodos.clicar(el.btnLogin);
	}

	@After
	public void finalizarTeste() {
		Executa.fecharNavegador();
	}

	@Given("clicar em carrinho")
	public void clicarEmCarrinho() {
		metodos.clicar(el.btnCarrinho);
	}

	@Given("clicar em checkout")
	public void clicarEmCheckout() {
		metodos.clicar(el.btnCheckout);
	}

	@Given("preencher minhas informacoes")
	public void preencherMinhasInformacoes() {
		metodos.escrever(el.firstName, massa.first_name);
		metodos.escrever(el.lastName, massa.last_name);
		metodos.escrever(el.postalCode, massa.postal_code);
		metodos.clicar(el.btnContinue);
	}

	@Given("validar informacoes da compra")
	public void validarInformacoesDaCompra() {
		metodos.validarTexto(el.paymentInformation, "SauceCard #31337");
		metodos.validarTexto(el.shippingInformation, "Free Pony Express Delivery!");
	}

	@When("clicar em finish")
	public void clicarEmFinish() {
		metodos.clicar(el.btnFinish);
	}

	@Then("compra realizada com sucesso")
	public void compraRealizadaComSucesso() {
		metodos.validarTexto(el.msgDeCompra, "Thank you for your order!");
		metodos.evidencia("Compra valida");
	}

	
	@Given("nao preencher campos obrigatorios")
	public void naoPreencherCamposObrigatorios() {
		metodos.escrever(el.firstName, "");
		metodos.escrever(el.lastName, "");
		metodos.escrever(el.postalCode, "");
		
	}
	
	@When("clicar em continue")
	public void clicarEmContinue() {
	    metodos.clicar(el.btnContinue);
	}

	
	@Then("aparece mensagem de erro")
	public void apareceMensagemDeErro() {
		metodos.validarTexto(el.msgFirstNameRequired, msg.msgFirstNameRequired);
		metodos.evidencia("Compra com dados em branco");
	}
	
	

	@When("clicar em cancel")
	public void clicarEmCancel() {
		metodos.clicar(el.btnCancel);
	}

	@Then("sou redirecionada para pagina principal")
	public void souRedirecionadaParaPaginaPrincipal() {
		metodos.validarTexto(el.products, "Products");
	}

	
}
