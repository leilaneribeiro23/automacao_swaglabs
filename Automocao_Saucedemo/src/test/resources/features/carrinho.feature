
@carrinho
Feature: Testar funcionalidade carrinho
  Como usuario
  Quero acessar o site
  Para testar a funcionalidade do carrinho
  
  Background: Acessar a tela de produtos
  Given que esteja na tela de produtos

  @adicionarProduto
  Scenario: Adicionar produto no carrinho
    And clicar no produto backpack
    And validar as informacoes do produto backpack
    And clicar em adicionar produto no carrinho
    When clicar no botao carrinho
    Then carrinho exibe produto adicionado

	@continuarCompra
	Scenario: Adicionar produto no carrinho e continuar compra
    And clicar no produto tshirt red
    And validar as informacoes do produto tshirt red
    And clicar em adicionar produto no carrinho
    And clicar no botao carrinho
    When clicar no botao continuar compra
    Then sou redirecionado para pagina inicial
    
    	@removerProduto
	Scenario: Remover produto do carrinho
    And adicionar produtos no carrinho
    And clicar no botao carrinho
    When clicar no botao remover
    Then carrinho atualizado com sucesso
    
  
