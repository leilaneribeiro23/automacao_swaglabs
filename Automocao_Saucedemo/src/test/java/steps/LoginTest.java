package steps;

import elementos.Elementos;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import metodos.Metodos;
import runner.Executa;
import utils.DicionarioDeMensagens;
import utils.MassaDeDados;

public class LoginTest {

	Metodos metodos = new Metodos();
	Elementos el = new Elementos();
	MassaDeDados massa = new MassaDeDados();
	DicionarioDeMensagens msg = new DicionarioDeMensagens();

	@Given("que esteja na tela de login")
	public void queEstejaNaTelaDeLogin() {
		Executa.abrirNavegador();
	}

	@After
	public void finalizarTeste() {
		Executa.fecharNavegador();
	}

	@Given("preencher o usuario")
	public void preencherOUsuario() {
		metodos.escrever(el.name, massa.standard_user);
	}

	@Given("preencher a senha")
	public void preencherASenha() {
		metodos.escrever(el.password, massa.secret_sauce);
	}

	@When("realizar login")
	public void realizarLogin() {
		metodos.clicar(el.btnLogin);
	}

	@Then("login realizado com sucesso")
	public void loginRealizadoComSucesso() {
		metodos.currentUrl("https://www.saucedemo.com/inventory.html");
		metodos.evidencia("Login realizado com sucesso");
	}

	@Given("preencher com usuario bloqueado")
	public void preencherComUsuarioBloqueado() {
		metodos.escrever(el.name, massa.locked_out_user);
	}

	@Then("sistema apresenta mensagem de usuario bloqueado")
	public void sistemaApresentaMensagemDeUsuarioBloqueado() {
		metodos.validarTexto(el.msgBlock, msg.msgBlock);
		metodos.evidencia("Mensagem de usuario bloqueado");
	}

	@Given("preencher com usuario invalido")
	public void preencherComUsuarioInvalido() {
		metodos.escrever(el.name, massa.problem_user);
	}

	@Given("preencher a senha invalida")
	public void preencherASenhaInvalida() {
		metodos.escrever(el.password, massa.senha_invalida);
	}

	@Then("sistema apresenta mensagem de usuario e senha invalida")
	public void sistemaApresentaMensagemDeUsuarioESenhaInvalida() {
		metodos.validarTexto(el.msgInvalida, msg.msgInvalido);
		metodos.evidencia("Usuario e senha invalido");
	}

	@Given("não preencher nenhum dado")
	public void nãoPreencherNenhumDado() {
		metodos.clicar(el.btnLogin);
	}

	@Then("sistema apresenta mensagem de dados em branco")
	public void sistemaApresentaMensagemDeDadosEmBranco() {
		metodos.validarTexto(el.msgBranco, msg.msgBranco);
		metodos.evidencia("Dados em branco");
	}

}
