package elementos;

import org.openqa.selenium.By;

public class Elementos {
	
	//Login
	public By name = By.id("user-name");
	public By password = By.id("password");
	public By btnLogin = By.id("login-button");
	
	//produtos
	public By backPack = By.xpath("//div[contains(text(),'Sauce Labs Backpack')]");
	public By bikeLight = By.xpath("//div[contains(text(),'Sauce Labs Bike Light')]");
	public By boltTShirt = By.xpath("//div[contains(text(),'Sauce Labs Bolt T-Shirt')]");
	public By fleeceJacket = By.xpath("//div[contains(text(),'Sauce Labs Fleece Jacket')]");
	public By onesie = By.xpath("//div[contains(text(),'Sauce Labs Onesie')]");
	public By tShirtRed = By.xpath("//div[contains(text(),'Test.allTheThings() T-Shirt (Red)')]");
	
	//informacoesprodutos
	 public By infoProdutos = By.xpath("//div[@class='inventory_details_desc large_size']");
	 
	
	//AddProdutos
	public By addBackPack = By.id("add-to-cart-sauce-labs-backpack");
	public By addBikeLight = By.id("add-to-cart-sauce-labs-bike-light");
	public By addBoltTShirt = By.id("add-to-cart-sauce-labs-bolt-t-shirt");
	public By addFleeceJacket = By.id("add-to-cart-sauce-labs-fleece-jacket");
	public By addOnesie = By.id("add-to-cart-sauce-labs-onesie");
	public By addTShirtRed = By.id("add-to-cart-test.allthethings()-t-shirt-(red)");
	
	
	public By removeBackPack = By.id("remove-sauce-labs-backpack");
	
	public By btnCarrinho = By.xpath("//a[@class='shopping_cart_link']");
	public By btnCheckout = By.id("checkout");
	public By btnContinueShopping = By.id("continue-shopping");
	
	public By firstName = By.id("first-name"); 
	public By lastName = By.id("last-name");
	public By postalCode = By.id("postal-code");
	public By btnContinue = By.id("continue");
	public By btnCancel = By.id("cancel");
	public By paymentInformation = By.xpath("//div[contains(text(),'SauceCard #31337')]");
	public By shippingInformation = By.xpath("//div[contains(text(),'Free Pony Express Delivery!')]");
	public By priceTotal = By.xpath("//*[@id=\"checkout_summary_container\"]/div/div[2]/div[8]/text()[2]");
	public By btnFinish = By.id("finish");
	public By msgDeCompra = By.xpath("//*[contains(text(),'Thank you for your order!')]");
	public By btnBackToProducts = By.id("back-to-products");
	public By products = By.xpath("//span[@class='title']");
	
	
	
	//Mensagens
	public By msgBlock = By.xpath("//*[text()='Epic sadface: Sorry, this user has been locked out.']");
	public By msgInvalida = By.xpath("//*[text()='Epic sadface: Username and password do not match any user in this service']");
    public By msgBranco = By.xpath("//*[text()='Epic sadface: Username is required']");
    public By msgFirstNameRequired = By.xpath("//*[text()='Error: First Name is required']");
 
    		
}

