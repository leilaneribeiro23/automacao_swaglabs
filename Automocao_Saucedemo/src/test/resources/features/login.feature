
@login
Feature: Login
  Como usuario
  Quero realizar o login
  Para acessar a conta
  
  Background: Acessar a tela de login
  Given que esteja na tela de login

  @loginPositivo
  Scenario: Login valido
    And preencher o usuario
    And preencher a senha
    When realizar login
    Then login realizado com sucesso

	@loginBlock
	Scenario: User bloqueado
   	But preencher com usuario bloqueado
    And preencher a senha
    When realizar login
    Then sistema apresenta mensagem de usuario bloqueado
  
  @loginInvalido  
  Scenario: Login com dados invalidos
  	But preencher com usuario invalido
   	But preencher a senha invalida
    When realizar login
    Then sistema apresenta mensagem de usuario e senha invalida
    
  @loginBranco
  Scenario: Login com dados em branco
    But não preencher nenhum dado
  	When realizar login
    Then sistema apresenta mensagem de dados em branco
