
@compra
Feature: Testar funcionalidade compra
  Como usuario
  Quero acessar o site
  Para efetuar uma compra
  
  Background: Acessar o site
  Given que esteja logado no site

  @compraValida
  Scenario: Compra valida
    And adicionar produtos no carrinho
    And clicar em carrinho
    And clicar em checkout
    And preencher minhas informacoes
    And validar informacoes da compra
    When clicar em finish
    Then compra realizada com sucesso
    
    @compraSemInformaçõesObrigatorias
  Scenario: Compra com dados em branco
    And adicionar produtos no carrinho
    And clicar em carrinho
    And clicar em checkout
    But nao preencher campos obrigatorios
    When clicar em continue
    Then aparece mensagem de erro
    
     @compraCancelada
  Scenario: Compra cancelada
    And adicionar produtos no carrinho
    And clicar em carrinho
    And clicar em checkout
    And preencher minhas informacoes
    When clicar em cancel
    Then sou redirecionada para pagina principal

	